This repo exists to track my Kotlin learning progress with *AtomicKotlin*, by Bruce Eckel and Svetlana Isakova.

To do the same:

1. Get the [IntelliJ CE](https://www.jetbrains.com/education/download/)
   free education bundle or add the Edutools plugin to an existing
   installation
2. Follow [this
   guide](https://kotlinlang.org/docs/tutorials/edu-tools-learner.html) to add
   AtomicKotlin to the Edutools plugin
3. Check out the [AtomicKotlin book
   sample](https://www.atomickotlin.com/sample/)

